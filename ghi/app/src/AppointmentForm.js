import React, {useState, useEffect} from 'react';



function AppointmentForm() {
    const [vin, setVin] = useState("");
    const [customer , setCustomer ] = useState("");
    const [date, setDate] = useState("");
    const [time, setTime] = useState("");
    const [technician, setTechnician] = useState("");
    const [reason, setReason] = useState("");
    const [technicians, setTechnicians] = useState([]);
    const [goodSubmit, setGoodSubmit] = useState(false);

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (goodSubmit) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    async function getTechnicians() {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);

        }
    }

    useEffect(() => {
        getTechnicians();
    }, []);


    function handleVinChange(event) {
        const { value } = event.target;
        setVin(value);
    }

    function handleCustomerChange(event) {
        const { value } = event.target;
        setCustomer(value);
    }

    function handleDateChange(event) {
        const { value } = event.target;
        setDate(value);
    }

    function handleTimeChange(event) {
        const { value } = event.target;
        setTime(value);
    }
    function handleReasonChange(event) {
        const { value } = event.target;
        setReason(value);
    }
    function handleTechnicianChange(event) {
        const { value } = event.target;
        setTechnician(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const date_time = `${date}T${time}:00.000Z`;
        const data = {
            vin,
            customer,
            date_time,
            technician,
            reason
            };

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();

            setVin("");
            setCustomer("");
            setDate("");
            setTime("");
            setTechnician("");
            setReason("");
            setGoodSubmit(true);
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a service appointment</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateChange} value={date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTimeChange} value={time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                        <select value={technician} onChange={handleTechnicianChange} name="technician" id="technician" className="form-select" >
                            <option value="">Choose a technician</option>
                            {technicians.map(technician => {
                            return (
                                <option key={technician.employee_id} value={technician.id}>{technician.first_name}  {technician.last_name}</option>
                            )
                            })}
                        </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses} id="success-message">
                        Appointment successfully added!
                    </div>
                </div>
            </div>
      </div>
    )
}

export default AppointmentForm
