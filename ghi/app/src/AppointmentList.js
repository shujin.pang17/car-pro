import React, { useState, useEffect } from 'react';

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [automobileVins, setAutomobileVins] = useState([]);


    async function getAppointments() {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments.filter(appointment => appointment.status === "ACTIVE"));
        }
    }

    useEffect(() => {
        getAppointments();
    }, []);

    async function getAutomobiles() {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
        const vinList = [];
        data.autos.forEach(item => {
            vinList.push(item.vin);
        });
        setAutomobileVins(vinList);
        }
    }

    useEffect(() => {
        getAutomobiles();
    }, []);

    function formatDateAndTime(dateTime) {
        const formattedDate = new Date(dateTime).toLocaleDateString();
        const formattedTime = new Date(dateTime).toLocaleTimeString();
        return { formattedDate, formattedTime };
    }

    async function handleCancelAppointment(appointmentId) {
        const cancelUrl = `http://localhost:8080/api/appointments/${appointmentId}/cancel/`;
        const fetchConfig = {
          method: "put",
          headers: {
            'Content-Type': 'application/json',
          }
        }
        const response = await fetch(cancelUrl, fetchConfig);
        if (response.ok) {
          // Refresh the list of appointments after cancellation
          getAppointments();
        }
      }

      async function handleFinishAppointment(appointmentId) {
        const finishUrl = `http://localhost:8080/api/appointments/${appointmentId}/finish/`;
        const fetchConfig = {
          method: "put",
          headers: {
            'Content-Type': 'application/json',
          }
        }
        const response = await fetch(finishUrl, fetchConfig);
        if (response.ok) {
          // Refresh the list of appointments after finishing
          getAppointments();
        }
      }

    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">is VIP</th>
                        <th scope="col">Customer</th>
                        <th scope="col">Data</th>
                        <th scope="col">Time</th>
                        <th scope="col">Technician</th>
                        <th scope="col">Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        const { formattedDate, formattedTime } = formatDateAndTime(appointment.date_time);
                        const isVIP = automobileVins.includes(appointment.vin) ? "Yes" : "No";
                        return (
                            <tr key={appointment.href}>
                                <td>{appointment.vin}</td>
                                <td>{isVIP}</td>
                                <td>{appointment.customer}</td>
                                <td>{formattedDate}</td>
                                <td>{formattedTime}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <button className="button-cancel" onClick={() => handleCancelAppointment(appointment.id)}>Cancel</button>
                                    <button className="button-finish" onClick={() => handleFinishAppointment(appointment.id)}>Finish</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>

    )
}

export default AppointmentList;
