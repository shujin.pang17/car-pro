import React, {useState} from 'react';

function CustomerForm() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phone, setPhone] = useState("");
    const [goodSubmit, setGoodSubmit] = useState(false);

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (goodSubmit) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    function handleFirstChange(event) {
        const { value } = event.target;
        setFirstName(value);
    }

    function handleLastChange(event) {
        const { value } = event.target;
        setLastName(value);
    }

    function handleAddressChange(event) {
        const { value } = event.target;
        setAddress(value);
    }

    function handlePhoneChange(event) {
        const { value } = event.target;
        setPhone(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phone;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {

            setFirstName("");
            setLastName("");
            setAddress("");
            setPhone("");
            setGoodSubmit(true);
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstChange} value={firstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastChange} value={lastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAddressChange} value={address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePhoneChange} value={phone} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone Number...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses} id="success-message">
                        Customer successfully added!
                    </div>
                </div>
            </div>
      </div>
    )
}

export default CustomerForm
