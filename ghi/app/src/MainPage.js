import animationData from "./carAnimation.json";
import { Player } from "@lottiefiles/react-lottie-player";
import Slooogan from "./typing";

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold custom-title-size">CarPro</h1>
      <div className="col-lg-6 mx-auto">
      <Slooogan />
      </div>
      <div className="col">
          <Player src={animationData} loop autoplay />
      </div>
    </div>
  );
}

export default MainPage;
