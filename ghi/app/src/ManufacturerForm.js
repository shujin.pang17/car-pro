import React, { useState } from 'react';

function ManufacturerForm(props) {
  const [name, setName] = useState('');
  const [created, setCreated] = useState(false);
  


  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
    name,
    };

    const Url = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(Url, fetchConfig);
    if (response.ok) {
      setName('');
      setCreated(true);
    }
  }

  function handleNameChange(event) {
    const { value } = event.target;
    setName(value);
  }


  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (created) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Manufacturer</h1>
          <form className={formClasses} onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
          <div className={messageClasses} id="success-message">
             Manufacturer successfully added!
        </div>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
