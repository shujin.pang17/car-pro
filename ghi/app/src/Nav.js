import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark custom-navbar">
      <div className="container-fluid">
        <div className="d-flex justify-content-evenly flex-wrap">
          <NavLink className="navbar-brand custom-nav-link" to="/">Home</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/manufacturers">Manufacturers</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/manufacturers/create">Add a Manufacturer</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/models">Models</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/vehicles/create">Add a Model</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/automobiles">Automobiles</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/automobiles/create">Add an Automobile</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/salespeople">Salespeople</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/salesperson/create">Add a Salesperson</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/customers">Customers</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/customers/create">Add a Customer</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/sales">Sales</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/sales/create">Add a Sale</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/salespersonhistory">Sales History</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/technicians">Technicians</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/technicians/create">Add A Technician</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/appointments">Service Appointments</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/appointments/create">Create a Service Appointment</NavLink>
          <NavLink className="navbar-brand custom-nav-link" to="/appointments/history">Service History</NavLink>
        </div>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
