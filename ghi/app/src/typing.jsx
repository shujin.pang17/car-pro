import { TypeAnimation } from "react-type-animation";

const Slooogan = () => {
  return (
    <TypeAnimation
      sequence={[
          "Revolutionize Your Dealership: One App, Endless Solutions!",
          2000,
          "Unleash Your Dealership's Potential!",
          2000,
      ]}
      wrapper="p"
      speed={30}
      repeat={Infinity}
      className="slogan"
    />
  );
};

export default Slooogan;
