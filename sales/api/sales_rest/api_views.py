from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale
import requests
from django.shortcuts import get_object_or_404

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "id",
    ]

    def get_extra_data(self, o):
        return {
            "salesperson": {
                "first_name": o.salesperson.first_name,
                "last_name": o.salesperson.last_name,
                "employee_id": o.salesperson.employee_id,
            },
            "customer": {
                "first_name": o.customer.first_name,
                "last_name": o.customer.last_name,
            },
            "automobile_vin": o.automobile.vin,
        }


@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        person_or_people = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": person_or_people},
            encoder=SalespersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        new_salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            new_salesperson,
            encoder=SalespersonListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, pk):
    if request.method == "DELETE":
        count, _ = get_object_or_404(Salesperson ,id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder=CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)
        new_customer = Customer.objects.create(**content)
        return JsonResponse(
            new_customer,
            encoder=CustomerListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    if request.method == "DELETE":
        count, _ = get_object_or_404(Customer, id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            if automobile.sold == True:
                return JsonResponse(
                    {"message": "Invalid automobile vin, this automobile has already been sold"},
                    status=400,
                )
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=400,
            )
        sale = Sale.objects.create(**content)

        # PUT request to update the sold status of an automobile to true for the Inventory API
        put_data = json.dumps({"sold": True})
        automobile_url = 'http://car-pro-inventory-api-1:8000/api/automobiles/' + automobile_vin + '/'
        response = requests.put(automobile_url, put_data)

        return JsonResponse(
            sale,
            encoder=SalesListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_sale(request, pk):
    if request.method == "DELETE":
        count, _ = get_object_or_404(Sale, id=pk).delete()
        return JsonResponse({"deleted": count > 0})
